'use strict';
const Hapi   = require('hapi');
const Server = new Hapi.Server();
const Saludo  = require('./lib/saludos');

Server.connection({ port: 3000 });

Server.route({
    method: 'GET',
    path: '/saludo/{user}',
    handler: function (request, reply) {

        const result = Saludo(decodeURIComponent(request.params.user));
        reply(result);
    }
});


if (!module.parent) {

    Server.start((err) => {

        if (err) {
            throw err;
        }
        console.log(`server corriendo en : ${Server.info.uri}`);
    });
}

module.exports = Server;
