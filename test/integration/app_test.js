'use strict';
const Code   = require('code');
const Lab    = require('lab');
const lab    = exports.lab = Lab.script();
const Server = require('../../app.js');

lab.experiment('Prueba integracion Url', () => {

    lab.test('Test: /saludo/Mundo ', (done) => {

        const options = {
            method: 'GET',
            url: '/saludo/Mundo'
        };

        Server.inject(options, ( response ) => {

            Code.expect(response.statusCode).to.equal(200);
            Code.expect(response.result).to.equal('Hola, Mundo!');
            done();
        });
    });

    lab.test('Greets /saludo/Mundo Cruel ', (done) => {

        const options = {
            method: 'GET',
            url: '/saludo/Mundo%20Cruel'
        };

        Server.inject(options, ( response ) => {

            Code.expect(response.statusCode).to.equal(200);
            Code.expect(response.result).to.equal('Hola, Mundo Cruel!');
            done();
        });
    });

});
