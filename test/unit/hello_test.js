'use strict';
const Code  = require('code');
const Lab   = require('lab');
const lab   = exports.lab = Lab.script();
const Saludo = require('../../lib/saludos.js');

lab.experiment('test greetings', () => {

    lab.test('Probando funcion ', (done) => {

        Code.expect(Saludo('Mundo')).to.equal('Hola, Mundo!');
        done();
    });

});
